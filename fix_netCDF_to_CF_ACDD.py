#!/usr/bin/env python3


'''
Turn old NetCDF files into standardized ones. Some unclear parts are marked with TODO. Run with --help to get info on usage.
'''


import sys
import os
import re
import json
import random
import hashlib
import logging
import shutil
from datetime import datetime, timedelta
from glob import glob

import jdcal
import netCDF4
import numpy as np


logging.basicConfig(format='%(levelname)s: %(message)s')


def fix_file(filename, cruise_name, outdir):
	'''Read, fix and write the given netCDF4 file'''

	nc = netCDF4.Dataset(filename)

	print('    Working on {} ...'.format(filename))

	tempfilename = '/tmp/{}'.format(rand_string(size=12))
	outfile = netCDF4.Dataset(tempfilename, 'w', format='NETCDF4', diskless=not outdir)

	dim_mappings = generate_dimensions(nc, outfile)

	variables = generate_variables(nc, outfile, dim_mappings)

	# This is a fix in the data itself. The raw data is lost and Laura de Steur found out that this particular fix has to be applied to these particular files.
	if get_max_date(outfile, 'year') == '2002' and filename.rsplit('.', maxsplit=1)[0].rsplit('/', maxsplit=1)[1] in {'rcm10071', 'rcm11059', 'rcm11475', 'rcm11625', 'rcm7718', 'rcm9464'}:
		special_direction_fix(nc, outfile)

	# The filename is needed for global attributes (id) but needs variables (time)
	outfilename = os.path.join(outdir, generate_filename(nc, cruise_name, os.path.basename(filename), outfile))

	global_attributes = generate_global_attributes(nc, outfile, outfilename, os.path.basename(filename), cruise_name)


	nc.close()
	outfile.close()

	if outdir:
		os.makedirs(outdir, exist_ok=True)
		json_outfilename = outfilename.rsplit('.', maxsplit=1)[0] + '.json'

		if os.path.isfile(outfilename):
			logging.warn('Filename already existing: {}'.format(outfilename))
			outfilename = '{}_{}.nc'.format(outfilename.rsplit('.', maxsplit=1)[0], rand_string())
			json_outfilename = outfilename.rsplit('.', maxsplit=1)[0] + '.json'

		shutil.move(tempfilename, outfilename)

		print('    ... saving to {}'.format(outfilename))
	else:
		print('    ... discarding result. Please run with -o some/path to save result.')


def rand_string(size=6, chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    '''Creates a random string of `size` length and `chars` letters.'''

    return ''.join(random.choice(chars) for _ in range(size))


def generate_dimensions(nc, outfile):
	mapping = {
		'time': 'T',
		'tim': 'T',
		'latitude': 'Y',
		'lat': 'Y',
		'longitude': 'X',
		'lon': 'X',
		'InstrDepth': 'Z',
		'DEPT': 'Z',
		'z': 'Z',
		'pressure': 'Z',
		'PRES': 'Z',
	}

	ignore = {'instrtype'}

	dimensions = {}

	todo_keys = []
	key_mapping = {}

	for key in nc.dimensions.keys():
		if key in ignore:
			continue
		elif key in mapping:
			dimensions[mapping[key]] = nc.dimensions.get(key).size
			key_mapping[key] = mapping[key]
		else:
			todo_keys.append(key)

	for key in todo_keys:
		size = nc.dimensions.get(key).size
		for d, s in dimensions.items():
			if size == s:
				key_mapping[key] = d
				break
		else:
			logging.error('The following dimension could not be mapped on any of TXYZ: {}'.format(key))

	for key, size in dimensions.items():
		outfile.createDimension(key, size)

	return key_mapping


def generate_global_attributes(nc, outfile, outfilename, filename, cruise_name):

	# Global attribute names that are taken care of (lower case)
	ok = {
		'calibration',
		'comment01',
		'comment02',
		'comment03',
		'comment04',
		'comment05',
		'date',
		'data_origin',
		'latitude',
		'longitude',
		'missing_value',
		'mooring',
		'instrument_type',
		'inst_type',
		'serie',
		'serial_number',
		'serialnumber',
		'start_date',
		'stop_date',
	}

	# Global attribute names that can directly be mapped to other names
	attr_mapping = {
		'water_depth': 'sea_floor_depth_below_sea_surface'
	}

	instrument_type = get_instrument_type(nc, filename, True)

	old_attrs = set(nc.ncattrs())

	if hasattr(nc, 'Calibration') and nc.Calibration != 'none':
		log.error('Calibration attribute present and not "none": {}'.format(nc.Calibration))


	attributes = {}
	attributes['id'] = generate_hash(outfilename)

	if hasattr(nc, 'title'):
		title = nc.title
		old_attrs.discard('title')
	else:
		title = 'Framstrait {}-{} time series from {}'.format(get_min_date(outfile, 'year'), get_max_date(outfile, 'year'), get_instrument_name(nc, filename))

	attributes['title'] = title

	### TODO: What to use as summary?
	summary = title
	attributes['summary'] = summary

	history = []
	history.append('{} device retrieved'.format(get_max_date(outfile)))
	history.append('data processed')
	history.append('{} file reformatted to NetCDF CF-1.6/ACDD-1.3'.format(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')))
	attributes['history'] = '\n'.join(history)

	attributes['comment'] = ''
	delim = ''
	for num in range(1, 10):
		if hasattr(nc, 'Comment0{}'.format(num)):
			attributes['comment'] += '{}{}'.format(delim, nc.getncattr('Comment0{}'.format(num)))
			delim = '; '
		else:
			break

	attributes['license'] = 'CC-BY 4.0'
	attributes['institution'] = 'Norwegian Polar Institute (NPI)'
	attributes['data_assembly_center'] = 'NO/NPI'
	attributes['Conventions'] = 'CF-1.7, ACDD-1.3'
	##### TODO: Ask Laura what this should be:
	attributes['processing_level'] = 'Instrument data that has been converted to geophysical values'
	attributes['date_created'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
	attributes['instrument_type'] = instrument_type
	instr_name = get_instrument_name(nc, filename, True)
	if instr_name:
		attributes['instrument'] = instr_name

	attributes['source'] = get_type(instrument_type)

	if get_type(instrument_type) == 'mooring':
		snum = get_serial_number(nc, filename, True)
		if snum:
			attributes['serial_number'] = snum

		site_code = get_site_code(nc, filename, outfile, True)
		if site_code:
			attributes['site_code'] = site_code

		attributes['featureType'] = 'timeSeries'
		attributes['keywords_vocabulary'] = 'GCMD Science Keywords'

		currentMeters = [
				'Recording Doppler Current Meter (RDCP)',
				'Recording Current Meter (RCM)',
				'Doppler Current Meter (DCM)']

		if instrument_type in currentMeters:
			attributes['keywords'] = 'EARTH SCIENCE>OCEANS>OCEAN CIRCULATION>OCEAN CURRENTS'
		elif instrument_type == 'Upward Looking Sonar':
			attributes['keywords'] = 'EARTH SCIENCE>OCEANS>SEA ICE>SEA ICE CONCENTRATION'
		else:
			attributes['keywords'] = 'EARTH SCIENCE>OCEANS>OCEANS TEMPERATURE,EARTH SCIENCE>OCEANS>SALINITY/DENSITY'

	if get_type(instrument_type) == 'ctd':
		attributes['cruise'] = get_cruise_name(nc, cruise_name)
		attributes['platform_code'] = get_platform_name(nc)
		attributes['site_code'] = str(get_station(nc))
		attributes['featureType'] = 'profile'
		attributes['keywords_vocabulary'] = 'GCMD Science Keywords'
		attributes['keywords'] = 'EARTH SCIENCE>OCEANS>OCEANS TEMPERATURE,EARTH SCIENCE>OCEANS>SALINITY/DENSITY'

	attributes['geospatial_vertical_positive'] = 'down'
	attributes['geospatial_vertical_min'] = get_min_vert(outfile)
	attributes['geospatial_vertical_max'] = get_max_vert(outfile)
	attributes['geospatial_lat_units'] = 'degree_north'
	attributes['geospatial_lat_min'] = get_min_lat(nc)
	attributes['geospatial_lat_max'] = get_max_lat(nc)
	attributes['geospatial_lon_units'] = 'degree_east'
	attributes['geospatial_lon_min'] = get_min_lon(nc)
	attributes['geospatial_lon_max'] = get_max_lon(nc)
	attributes['time_coverage_start'] = get_min_date(outfile)
	attributes['time_coverage_end'] = get_max_date(outfile)

	attributes['publisher_name'] = 'Norwegian Polar Institute (NPI)'
	attributes['iso_topic_category'] = 'oceans'
	attributes['naming_authority'] = 'npolar.no'
	attributes['standard_name_vocabulary'] = 'CF Standard Name Table v69'


	while old_attrs:
		attr = old_attrs.pop()
		if attr.lower() in attr_mapping:
			if not np.isnan(nc.getncattr(attr)):
				attributes[attr_mapping[attr.lower()]] = nc.getncattr(attr)
		elif attr.lower() not in ok:
			logging.warn('The attribute "{}" is not recognized!'.format(attr))


	for k, v in sorted(attributes.items()):
		try:
			outfile.setncattr(k, v)
		except TypeError:
			logging.critical('{} is {}'.format(k, v))
			#raise


def determine_dimension(nc, var, dim_mappings):
	dims = nc.variables[var].dimensions
	if len(dims) == 1:
		return dim_mappings[dims[0]]
	elif len(dims) == 2:
		if nc.dimensions.get(dims[0]).size == 1 and nc.dimensions.get(dims[1]).size > 1:
			return dim_mappings[dims[1]]
		elif nc.dimensions.get(dims[0]).size > 1 and nc.dimensions.get(dims[1]).size == 1:
			return dim_mappings[dims[0]]
		else:
			raise NotImplementedError('Found two dimensions, where both are > 1')


	raise NotImplementedError('Found more than two dimensions')


def generate_variables(nc, outfile, dim_mappings):
	ignore = {'type', 'serialnumber'}

	var_keys = set(nc.variables.keys())

	# Make sure, the time variable is created first, since it may be used by other variables.
	if 'tim' in var_keys:
		var = 'tim'
	elif 'time' in var_keys:
		var = 'time'
	else:
		raise ValueError('No time variable found!')
	dim = determine_dimension(nc, var, dim_mappings)
	create_var_time(var, dim, nc, outfile)
	var_keys.remove(var)


	for var in var_keys:
		if var in ignore:
			continue

		dim = determine_dimension(nc, var, dim_mappings)

		if var in {'latitude', 'lat'}:
			create_var_lat(var, dim, nc, outfile)
		elif var in {'longitude', 'lon'}:
			create_var_lon(var, dim, nc, outfile)
		elif var in {'salinity', 'S'}:
			create_var_salinity(var, dim, nc, outfile)
		elif var in {'temperature', 'TEMP'}:
			create_var_temperature(var, dim, nc, outfile)
		elif var in {'pressure', 'PRES'}:
			create_var_pressure(var, dim, nc, outfile)
		elif var in {'depth', 'DEPTH', 'DEPT', 'Instr_Depth', 'InstrDepth', 'z'}:
			create_var_depth(var, dim, nc, outfile)
		elif var == 'conductivity':
			create_var_conductivity(var, dim, nc, outfile)
		elif var == 'uvel':
			create_var_u_current(var, dim, nc, outfile)
		elif var == 'vvel':
			create_var_v_current(var, dim, nc, outfile)
		elif var == 'dire':
			create_var_direction(var, dim, nc, outfile)
		elif var == 'velocity':
			create_var_speed(var, dim, nc, outfile)
		elif var == 'tilt':
			create_var_tilt(var, dim, nc, outfile)
		elif var.startswith('traveltime'):
			create_var_travel_time(var, dim, nc, outfile)
		elif var.startswith('TTenvelope'):
			create_var_TT_envelope(var, dim, nc, outfile)
		else:
			logging.error('Variable "{}" is unknown.'.format(var))


def generate_hash(string):
	return hashlib.md5(bytes(string, 'utf-8')).hexdigest()


def special_direction_fix(nc, outfile):
	# This is a fix in the data itself. The raw data is lost and Laura de Steur
	# found out that this particular fix has to be applied to these particular
	# files. It adds 12.5 to all directions (CDIR) and recalculates UCUR and
	# VCUR based on HCSP (velocity) and the new CDIR.

	logging.info('Running special direction fix!')

	dir_var = outfile.variables['CDIR']
	dir_var[:] = fix_fill_value(nc, 'dire', nc.variables['dire'][:], 99999) + 12.5
	return
	uvel_var = outfile.variables['UCUR']
	vvel_var = outfile.variables['VCUR']
	vel_var = outfile.variables['HCSP']

	uvel_var[:] = vel_var[:] * np.sin(np.radians(dir_var[:]))
	uvel_var[:] = vel_var[:] * np.cos(np.radians(dir_var[:]))


def fix_fill_value(nc, key, array, fill_value):
	v = nc.variables[key]
	replace = np.nan

	if hasattr(v, 'fill_value'): replace = v.fill_value
	elif hasattr(v, 'missing_value'): replace = v.missing_value
	elif hasattr(v, 'missingvalue'): replace = v.missingvalue
	elif hasattr(v, 'fillvalue'): replace = v.fillvalue
	elif hasattr(nc, 'fill_value'): replace = nc.fill_value
	elif hasattr(nc, 'fillvalue'): replace = nc.fillvalue
	elif hasattr(nc, 'missing_value'): replace = nc.missing_value
	elif hasattr(nc, 'missingvalue'): replace = nc.missingvalue

	array[array == replace] = fill_value
	array[array == netCDF4.default_fillvals['f8']] = fill_value
	np.nan_to_num(array, copy=False, nan=fill_value)

	return array


def create_var_time(var, dim, nc, outfile):
	time_points = nc.variables[var][:]

	if time_points.size == 1 and np.isnan(time_points[0]):
		logging.critical('Invalid time!')
		return

	time_var = outfile.createVariable('TIME', 'f8', (dim,), zlib=True, complevel=9)
	time_var.standard_name = 'time'
	time_var.units = 'days since 1950-01-01T00:00:00Z'
	time_var.axis = 'T'
	time_var.long_name = 'measurement time'
	time_var.conventions = 'Relative julian days with decimal part'

	units = ''
	if hasattr(nc.variables[var], 'units'):
		units = nc.variables[var].units
	elif hasattr(nc.variables[var], 'time'):
		units = nc.variables[var].time
	elif hasattr(nc.variables[var], 'longname'):
		units = nc.variables[var].longname

	original = None

	if units:
		if 'days since 1.1.1940' in units or 'days since 1 1 1940' in units:
			original = jdcal.gcal2jd(1940, 1, 1)[1]
		elif 'days since 1.1.2000' in units:
			original = jdcal.gcal2jd(2000, 1, 1)[1]
		else:
			raise ValueError('Time unknown: {}'.format(units))
	### TODO: Negative time points should not happen without unit!
	elif time_points.min() < 0:
		if -2410000 < time_points.min() < -2400000:
			logging.error('Negative time points! Very unreliable!')
			original = jdcal.gcal2jd(1997, 3, 29)[1]
			time_points = [abs(t + jdcal.MJD_0) for t in time_points]
		else:
			raise ValueError('Time negative')
	else:
		raise ValueError('Unknown julian date format')

	offset = jdcal.gcal2jd(1950, 1, 1)[1]

	time_var[:] = [original + t - offset for t in time_points]

	if(time_var[0] < 0): ### TODO!!! Sometimes, the time is negative. This is wrong!
		logging.critical('TIME IS NEGATIVE')
		logging.critical('original:', original)
		logging.critical('offset:', offset)
		logging.critical('time_points[0]:', time_points[0])
		logging.critical('original + time_points[0] - offset:', original + time_points[0] - offset)


def create_var_lat(var, dim, nc, outfile):
	new_var = outfile.createVariable('LATITUDE', 'f8', (dim,), zlib=True, complevel=9)
	new_var.standard_name = 'latitude'
	new_var.long_name = 'latitude'
	new_var.units = 'degree_north'
	new_var.axis = 'Y'
	new_var.reference = 'WGS-84'
	new_var.valid_min = -90.0
	new_var.valid_max = 90.0
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = nc.variables[var][:]


def create_var_lon(var, dim, nc, outfile):
	new_var = outfile.createVariable('LONGITUDE', 'f8', (dim,), zlib=True, complevel=9)
	new_var.standard_name = 'longitude'
	new_var.long_name = 'longitude'
	new_var.units = 'degree_east'
	new_var.axis = 'X'
	new_var.reference = 'WGS-84'
	new_var.valid_min = -180.0
	new_var.valid_max = 180.0
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = nc.variables[var][:]


def create_var_salinity(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('PSAL', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'sea_water_practical_salinity'
	new_var.units = '1'
	new_var.reference_scale = 'PPS-78'
	new_var.long_name = 'salinity'
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_temperature(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('TEMP', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'sea_water_temperature'
	new_var.units = 'degree_Celsius'
	new_var.reference_scale =  'ITS-90'
	new_var.long_name = 'temperature'
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_depth(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('DEPTH', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'depth'
	new_var.units = 'meter'
	new_var.axis = 'Z' #### TODO: What happens if pressure and depth occur in the same file?
	new_var.long_name = 'measurement depth (instrument depth)'
	new_var.reference = 'sea_level'
	new_var.positive = 'down'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_pressure(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('PRES', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'sea_water_pressure'
	new_var.units = 'decibar'
	new_var.axis = 'Z'
	new_var.long_name = 'pressure'
	new_var.reference_scale = 'mean_sea_level'
	new_var.positive = 'down'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)

	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment


def create_var_conductivity(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('CNDC', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'sea_water_electrical_conductivity'
	new_var.units = 'millisiemens/centimeter'
	new_var.long_name = 'conductivity'
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


# Eastward current velocity measured by DCM and RCM sensors
def create_var_u_current(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('UCUR', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'eastward_sea_water_velocity'
	new_var.long_name = 'current east component'
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment

	try:
		unit = nc.variables[var].units
	except AttributeError:
		unit = nc.variables[var].uvel_units

	if unit == 'cm/s' or (unit == ' ' and 2004 <= int(get_max_date(outfile, 'year')) <= 2009):
		new_var.units = 'meter/second'
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value*100)*0.01 # turn cm into m, preserving fill_value
	else:
		logging.info('UVelocity unit is "{}"'.format(unit))
		new_var.units = unit
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


# Northward Current Velocity measured by DCM and RCM
def create_var_v_current(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('VCUR', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'northward_sea_water_velocity'
	new_var.long_name = 'current north component'
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment

	try:
		unit = nc.variables[var].units
	except AttributeError:
		unit = nc.variables[var].vvel_units

	if unit == 'cm/s' or (unit == ' ' and 2004 <= int(get_max_date(outfile, 'year')) <= 2009):
		new_var.units = 'meter/second'
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value*100)*0.01 # turn cm into m, preserving fill_value
	else:
		logging.info('VVelocity unit is "{}"'.format(unit))
		new_var.units = unit
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


# Current direction logged by RCM
def create_var_direction(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('CDIR', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'direction_of_sea_water_velocity'
	new_var.units = 'degree'
	new_var.long_name = 'direction'
	new_var.coverage_content_type = 'physicalMeasurement'
	new_var.valid_min = 0.0
	new_var.valid_max = 360.0
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_speed(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('HCSP', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'sea_water_speed'
	new_var.long_name = 'speed'
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment

	try:
		unit = nc.variables[var].units
	except AttributeError:
		unit = nc.variables[var].velocity_units

	if unit == 'cm/s' or (unit == ' ' and 2004 <= int(get_max_date(outfile, 'year')) <= 2009):
		new_var.units = 'meter/second'
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value*100)*0.01 # turn cm into m, preserving fill_value
	else:
		logging.info('Speed unit is "{}"'.format(unit))
		new_var.units = unit
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_tilt(var, dim, nc, outfile):
	fill_value = 99999
	new_var = outfile.createVariable('TILT', 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.units = 'degree'
	new_var.standard_name = 'sensor_view_angle'
	new_var.long_name = 'instrument tilt'
	new_var.coverage_content_type = 'physicalMeasurement'
	new_var.valid_min = 0.0
	new_var.valid_max = 360.0
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment
	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_travel_time(var, dim, nc, outfile):
	fill_value = 99999
	idx = int(var[-1])
	name = 'TTIME{}'.format(idx)
	new_var = outfile.createVariable(name, 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.standard_name = 'acoustic_signal_roundtrip_travel_time_in_sea_water'
	new_var.long_name = 'two way travel time number {}'.format(idx)
	new_var.coverage_content_type = 'physicalMeasurement'
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment

	unit = nc.variables[var].units

	if unit.lower() == 'ms':
		new_var.units = 'second'
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value*1000)*0.001 # turn ms into s, preserving fill_value
	else:
		logging.info('Travel time unit is "{}"'.format(unit))
		new_var.units = unit
		new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def create_var_TT_envelope(var, dim, nc, outfile):
	# This is a non-CF variable, so no standard_name is given.

	fill_value = 99999
	idx = int(var[-1])
	name = 'TTENV{}'.format(idx)
	new_var = outfile.createVariable(name, 'f8', (dim,), fill_value=fill_value, zlib=True, complevel=9)
	new_var.long_name = 'travel time envelope number {}'.format(idx)
	if hasattr(nc.variables[var], 'comment'):
		new_var.comment = nc.variables[var].comment

	new_var[:] = fix_fill_value(nc, var, nc.variables[var][:], fill_value)


def get_cruise_name(nc, cruise_name):
	name = cruise_name

	if hasattr(nc, 'cruise'):
		result = re.search(r'^fs(\d{4})$', nc.cruise)

		if result:
			name = 'Framstrait-{}'.format(m.group(1))
		else:
			name = nc.cruise

	return name


def get_platform_name(nc):
	if hasattr(nc, 'platform'):
		if nc.platform == 'lance':
			return 'R/V Lance'
		else:
			return nc.platform
	else:
		return ''


def get_min_lat(nc):
	try:
		return nc.variables['latitude'][:].min()
	except KeyError:
		return nc.variables['lat'][:].min()


def get_max_lat(nc):
	try:
		return nc.variables['latitude'][:].max()
	except KeyError:
		return nc.variables['lat'][:].max()


def get_min_lon(nc):
	try:
		return nc.variables['longitude'][:].min()
	except KeyError:
		return nc.variables['lon'][:].min()


def get_max_lon(nc):
	try:
		return nc.variables['longitude'][:].max()
	except KeyError:
		return nc.variables['lon'][:].max()


def get_min_vert(outfile):
	if 'PRES' in outfile.variables.keys():
		return outfile.variables['PRES'][:].min()
	if 'DEPTH' in outfile.variables.keys():
		return outfile.variables['DEPTH'][:].min()


def get_max_vert(outfile):
	if 'PRES' in outfile.variables.keys():
		return outfile.variables['PRES'][:].max()
	if 'DEPTH' in outfile.variables.keys():
		return outfile.variables['DEPTH'][:].max()


def get_min_date(outfile, format='ISO8601'):
	reference = jdcal.gcal2jd(1950, 1, 1)[1]
	jdmin = outfile.variables['TIME'][:].min()

	d = datetime(1950, 1, 1) + timedelta(days=jdmin)

	if format == 'year':
		return d.strftime('%Y')
	else:
		return d.strftime('%Y-%m-%dT%H:%M:%SZ')


def get_max_date(outfile, format='ISO8601'):
	reference = jdcal.gcal2jd(1950, 1, 1)[1]
	jdmax = outfile.variables['TIME'][:].max()

	d = datetime(1950, 1, 1) + timedelta(days=jdmax)

	if format == 'year':
		return d.strftime('%Y')
	else:
		return d.strftime('%Y-%m-%dT%H:%M:%SZ')


def get_instrument_type(nc, filename, show_error=False):
	'''Get a standardized instrument type'''

	instrument_name = get_instrument_name(nc, filename)

	ctd = {'SBE 911plus CTD', 'SBE 19plus SeaCAT CTD'}
	rcm = {'Aanderaa RCM 7', 'Aanderaa RCM 8', 'Aanderaa RCM 9', 'Aanderaa RCM 11'}
	ctp = {'SBE 16 SeaCAT', 'SBE 37-IM MicroCAT'}
	dcm = {'Aanderaa DCM 12'}
	uls = {'ES300', 'CMR ES300'}
	rdcp = {'Aanderaa RDCP600'}
	adcp = {'Workhorse Sentinel ADCP 300 KHz'}
	tcs = {'Data Logger DL-7 with string'}

	if instrument_name in uls:
		return 'Upward Looking Sonar'
	if instrument_name in rdcp:
		return 'Recording Doppler Current Meter (RDCP)'
	if instrument_name in ctd:
		return 'CTD'
	if instrument_name in rcm:
		return 'Recording Current Meter (RCM)'
	if instrument_name in ctp:
		return 'C-T(P) Recorder'
	if instrument_name in dcm:
		return 'Doppler Current Meter (DCM)'
	if instrument_name in adcp:
		return 'Acoustic Doppler current profiler (ADCP)'
	if instrument_name in tcs:
		return 'Thermistor-Conductivity string'

	if show_error:
		logging.error('Could not determine instrument type from name "{}"'.format(instrument_name))

	return instrument_name


def get_type(instrument_type):
	'''Get the type of measurement'''

	if instrument_type == 'CTD':
		return 'ctd'

	return 'mooring'


# All serial numbers where verified using cruise reports or rig diagrams for
# respective cruises or previous cruises.
def get_instrument_name(nc, filename, show_error=False):
	t = ''

	# Serial numbers recovered from cruise reports
	DCM12 = {17, 47, 190}
	RCM7 = {375, 11475, 11854, 10349, 7718, 12643, 12644, 12464, 11059, 9464, 10303, 11845, 9706, 9465, 2414, 8396, 10907, 9219, 9997}
	RCM8 = {12644, 10071, 11625, 12733, 10069}
	RCM9 = {834, 1046, 836, 1049, 1148, 1147, 1325, 1326, 1327, 1175}
	RCM11 = {218, 1423, 235, 228, 117, 372, 377, 384, 494, 538, 556, 561}
	SBE16 = {4321, 4322}
	SBE37 = {2963, 2962, 2813, 2814, 2967, 2942}

	if hasattr(nc, 'ctd'):
		t = str(nc.ctd)
	elif hasattr(nc, 'Instrument_Type'):
		t = str(nc.Instrument_Type)
	elif hasattr(nc, 'INST_TYPE'):
		t = str(nc.INST_TYPE)

	if t.lower() in {'0', 'rcm-aanderaa', 'avts', 'rcm', 'dcm'}:
		snum = get_serial_number(nc, filename)

		# Old files (fs1998) have serial numbers 17001 - 17006 and 47001 - 47007 for 17 and 47, respectively
		if t.lower() == 'dcm':
			if snum in range(17001, 17007):
				snum = 17
			elif snum in range(47001, 47007):
				snum = 47

		if snum in DCM12:
			t = 'dcm12'
		elif snum in RCM7:
			t = 'rcm7'
		elif snum in RCM8:
			t = 'rcm8'
		elif snum in RCM9:
			t = 'rcm9'
		elif snum in RCM11:
			t = 'rcm11'
		elif snum in SBE16:
			t = 'sbe16'
		elif snum in SBE37:
			t = 'sbe37'
		else:
			logging.warn('Instrument type "{}" with Serial Number "{}" not found!'.format(t, snum))

	elif t == 'unknown':
		# If none of the lists match extract the instrument prefix
		# from the filename and pass that on to the matcher function
		result = re.search(r'([a-z]{3}\d+).*\.nc$', filename)
		if result:
			t = tc.group(1)
		else:
			t = ''

	try:
		return {
			'sbe911plus'     : 'SBE 911plus CTD',
			'sbe911plus ctd' : 'SBE 911plus CTD',
			'sbe19plus'      : 'SBE 19plus SeaCAT CTD',
			'sbe19plus ctd'  : 'SBE 19plus SeaCAT CTD',
			'ES300'          : 'CMR ES300',
			'sbe16'          : 'SBE 16 SeaCAT',
			'Seabird 16'     : 'SBE 16 SeaCAT',
			'SEACAT'         : 'SBE 16 SeaCAT',
			'sbe37'          : 'SBE 37-IM MicroCAT',
			'SBE37-microcat' : 'SBE 37-IM MicroCAT',
			'microcat'       : 'SBE 37-IM MicroCAT',
			'MICRO_CPR'      : 'SBE 37-IM MicroCAT',
			'MICRO_CAT'      : 'SBE 37-IM MicroCAT',
			'WORKHORSE300'   : 'Workhorse Sentinel ADCP 300 KHz',
			'RDCP600'        : 'Aanderaa RDCP600',
			'rcm7'           : 'Aanderaa RCM 7',
			'rcm 7'          : 'Aanderaa RCM 7',
			'rcm8'           : 'Aanderaa RCM 8',
			'rcm9'           : 'Aanderaa RCM 9',
			'rcm11'          : 'Aanderaa RCM 11',
			'dcm12'          : 'Aanderaa DCM 12',
			'DCM12'          : 'Aanderaa DCM 12',
			'DL7 with string': 'Data Logger DL-7 with string'
		}[t]
	except KeyError:
		if show_error:
			logging.warn('Instrument type "{}" is unknown'.format(t))
		return t


def get_serial_number(nc, filename, show_error=False):
	'''Try to determine serial number.'''

	if hasattr(nc, 'Serialnumber'):
		return nc.Serialnumber

	if hasattr(nc, 'Serial_Number'):
		return nc.Serial_Number

	if hasattr(nc, 'SERIE'):
		return nc.SERIE

	result = re.search(r'(?:rcm|dcm|sbe)(\d+).*.nc$', filename)
	if result:
		return int(result.group(1))

	if show_error:
		logging.info('No serial number found')
	return 0


def get_site_code(nc, filename, outfile, show_error=False):
	'''Try to extract the site code from the file, comment field, or filename'''

	sc = ''

	if hasattr(nc, 'station'):
		sc = nc.station

	elif hasattr(nc, 'Mooring'):
		sc = nc.Mooring

	if sc:
		if len(sc) > 4:
			m = re.search(r'F\d\d', sc)
			if m:
				sc = m.group(0)
			else:
				logging.info('Site code seems wrong: {}'.format(sc))
				return ''

		return sc

	try:
		m = re.search(r'.*[fF][sS]?(\d{2})', nc.Comment01)
		if m:
			return 'F' + m.group(1)
		elif nc.Comment01 == 'NPI Fram Strait Mooring fsny':
			return 'FNY'
	except AttributeError:
		pass

	p = re.search(r'(F\d{2})-\d\/.*\.nc$', filename)
	if p:
		return p.group(1)

	# Site codes based on HTML overview by Laura
	if get_max_date(outfile, 'year') == '2004':
		if filename.startswith('dcm190_') or filename in {'rcm10071.nc', 'rcm1046.nc', 'rcm11475.nc', 'rcm228.nc', 'sbe4321.nc'}:
			return 'FS11'
		if filename in {'rcm11625.nc'}:
			return 'FS12'
		if filename.startswith('dcm17_') or filename in {'rcm12733.nc', 'rcm235.nc', 'rcm7718.nc'}:
			return 'FS13'
		if filename in {'rcm12644.nc', 'rcm834.nc', 'sbe2962.nc', 'sbe4322.nc'}:
			return 'FS14'
		if filename in {'sbe2942.nc', 'sbe2967.nc'}:
			return 'FS19'

	if show_error:
		logging.info('No site code found')
	return ''


def generate_filename(nc, cruise_name, filename, outfile, ext='nc'):
	'''Generate a filename based on metadata'''

	instrument_type = get_instrument_type(nc, filename)

	if instrument_type == 'CTD':
		return '{}_{}_{:03d}_{}.{}'.format(
			cruise_name,
			get_min_date(outfile, 'year'),
			get_site_code(nc, filename, outfile),
			get_type(instrument_type),
			ext
		)

	else: ## NB! Currently only mooring! No bottles/buoys!
		return '{}_{}-{}_{}_{}-{}.{}'.format(
			cruise_name,
			get_min_date(outfile, 'year'),
			get_max_date(outfile, 'year')[-2:], # only last two digits of the year
			get_site_code(nc, filename, outfile),
			get_type(instrument_type),
			filename.rsplit('.', maxsplit=1)[0],
			ext
		)


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('-n', '--name', help='Project name (e.g. Framstrait)')
	parser.add_argument('-d', '--directory', metavar='STR', help='Path to the directory to convert all files from')
	parser.add_argument('-f', '--file', metavar='STR', dest='fn',  help='Path to the file to convert')
	parser.add_argument('-o', '--outdir', metavar='STR', help='Output directory (will be created if not exists)')
	parser.add_argument('-q', '--quiet', action='store_true', help='Show less output')
	args = parser.parse_args()

	if not args.quiet:
		logging.getLogger().setLevel(logging.DEBUG)

	if not args.name:
		print('No name given', file=sys.stderr)
		parser.print_help(sys.stderr)
		sys.exit(1)

	if args.outdir:
		os.makedirs(args.outdir, exist_ok=True)
	else:
		args.outdir = ''

	if args.directory:
		for fn in sorted(glob('{}/*.nc'.format(args.directory))):
			fix_file(fn, args.name, args.outdir)
	elif args.fn:
		if not args.fn.endswith('.nc'):
			raise ValueError('Filename does not end with .nc')
		fix_file(args.fn, args.name, args.outdir)
	else:
		print('Neither file nor directory given', file=sys.stderr)
		parser.print_help(sys.stderr)
		sys.exit(1)
