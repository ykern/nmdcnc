Physical oceanography data at NPDC
==================================


Key points
----------

This section is an overview. Read on below for details.

1. Format your files as NetCDF4 following [CF-1.7](http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html) (or newer) and [ACDD-1.3](http://wiki.esipfed.org/index.php/Attribute_Convention_for_Data_Discovery) (or newer).
    - One file for each instrument for each **mooring**
    - One file for all **CTD**s of one cruise
    - One file for all **bottles** of one cruise
    - One file for each **buoy**
2. Name your file in a sensible way.
3. Upload the files to [NPDC](https://data.npolar.no/dataset).
    - As a rule of thumb, create one dataset per data type (CTD, mooring, ...) per cruise, so up to four datasets per cruise. Deviate if necessary.
4. The files will be automatically checked against CF and ACDD.
5. Different representations of the files will be available using OPeNDAP.

Cruise reports shall be uploaded to [Brage](https://brage.npolar.no) and referred to in the metadata.


Introduction
------------

*This section is non-normative*

This document contains suggestions for handling physical oceanography data at NPI. If you have comments or questions, please either write to data@npolar.no or create an “issue” on this page (in the toolbar on the left-hand side; you need a free Gitlab account to create issues).

In general, we strive to follow modern data management methods as, for example, defined by the [W3C](https://w3c.github.io/dwbp/bp.html). These methods separate between the data as an abstract model and multiple possible distributions (file formats in this case). So the long-term objective would be to allow the upload of data in one of many possible formats, save the data and metadata in some internal format, and allow the user to decide in which format they want to download the data.

The NPDC will use OPeNDAP to manage different distributions of the data. OPeNDAP will also allow users to download only parts of the data or the data of multiple files at once.

A minimal example file can be found for [mooring](mooring_example.cdl) and [CTD](ctd_example.cdl).


Data formats
------------

NetCDF4 is the common data format for any published final data, including moorings, CTDs, bottles, and buoys.

The granularity of the data must be considered for each project, but a good guideline may be the following:

- One file for each instrument for each **mooring**
- One file for all **CTD**s of one cruise
- One file for all **bottles** of one cruise
- One file for each **buoy**

Calibrated raw data should be uploaded “as is” to the same dataset as the final data. Software necessary to read the raw data should be mentioned and preferably linked to in the metadata. Intermediate data (e.g. Matlab files) should *not* be uploaded.

Cruise reports shall be uploaded to [Brage](https://brage.npolar.no) and linked to from the metadata via its *handle* (a link in the form `http://hdl.handle.net/11250/*`).

![Example for structuring of datasets and files](datasets.svg)


File names
----------

Filenames should contain the most important information. We suggest the following format:

```
projectOrCruise_StartYear-EndYear_stationType_stationOrPosition_instrumentName_serialNumber.nc
```

For example, a file with data from a Doppler current meter with the serial number 17 from station F14 in the Framstrait cruise 2004 to 2005 could have the name:

```
FS_2004-05_mooring_F14_dcm_17.nc
```


Metadata standards
------------------

All processed files should be in NetCDF4 format following the  [CF-1.7](http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html) (or newer) and [ACDD-1.3](http://wiki.esipfed.org/index.php/Attribute_Convention_for_Data_Discovery) (or newer) metadata conventions. The metadata conventions followed must be stated in the *CF* global attribute `Conventions`. Normally, this attribute should look like `:Conventions = "CF-1.7 ACDD-1.3" ;`. Every data provider may add other metadata conventions, but must add these to the `Conventions` attribute. Possible other relevant conventions are (not exhaustive) OceanSITES-1.x, NMDC-netCDF, and SeaDataNet_1.x.

To allow a common analysis of data from NPI and other organisations, the variable names should follow a common controlled vocabulary. CF hosts an extensive [*standard name* repository](http://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html) with such a vocabulary. Users should try to use these standard names for variables wherever applicable.

Upon upload, the NetCDF files will automatically be checked for correct implementation of *CF* and *ACDD* standards. Files not following the standards will be rejected with a human-readable error message.

The NPDC will also take care of exposing the metadata to aggregation services in appropriate formats.


Integration with the NPDC data catalogue
----------------------------------------

The physical oceanography data shall be integrated in the NPDC data catalogue. That is, we do not plan to create a dedicated database for physical oceanography data.

The organisation of data must be decided on a case-by-case basis. But as a rule of thumb, we consider one dataset per data type per cruise as a good organisation. So, for example, all CTD casts of one cruise would be one dataset.


Global attributes
-----------------

*This section is non-normative. It only summarizes the most important points of the CF and ACDD standards.*

Latitude and longitude must be given as global attributes `geospatial_lat_min`, `geospatial_lat_max`, `geospatial_lon_min`, and `geospatial_lon_max`, where min and max should be the same if all data points refer to the same coordinate (e.g. in the case of a single mooring instrument).

The time coverage of the dataset must be given using the global attributes `time_coverage_start` and `time_coverage_end`. Both attributes must be strings following [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) in UTC (e.g. 2019-06-19T05:38:52Z).

The creators should be given in the attribute `creator_name`, separated by semicolon.

The `license` (American English) should be set to “CC-BY 4.0”, optionally followed by an explanation or a link to [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/). This is the standard licence of NPDC. In short, it allows anyone to share the data (i.e. copy and redistribute the material in any medium or format) and also to adapt the data (i.e. remix, transform, and build upon the material for any purpose, even commercially). However, anyone using and publishing your data in any form has to give appropriate credit, provide a link to the license, and indicate if changes were made.


### CF

CF-1.7 *recommends* the following attributes, however, the NPDC considers these attributes *mandatory*.

| Name | Comment |
| ---- | ------- |
| Conventions | Space-separated name(s) of the convention(s) followed by the dataset. For example: “CF-1.7 ACDD-1.3”. Note the capital “C”! |
| title | A compact description of what is in the dataset. |
| history | A log of modifications to the file. One event per line, starting with an [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date-time. |
| institution | Institution where the original data was produced. Normally: “Norwegian Polar Institute (NPI)” |
| source | Method of production of the original data (no controlled vocabulary!) e.g. “mooring” |
| references | Published or web-based references that describe the data or methods used to produce the dataset. This should normally be a link or DOI to the NPDC website with the metadata of this dataset. |
| comment | Free text with miscellaneous information about the data, processing and additional calibration, or methods used to produce the dataset. |


### ACDD

For highly recommended, recommended, and suggested attributes for ACDD, please see [wiki.esipfed.org/index.php/Attribute_Convention_for_Data_Discovery](http://wiki.esipfed.org/index.php/Attribute_Convention_for_Data_Discovery#Global_Attributes).


### Often used global attributes not in any common standards

There are some attributes that were frequently used in older data (mostly seen in data from the Fram Strait after 2003). All standards considered in this document allow for arbitrary attributes, but users are strongly requested to use standardized attribute names for our local use at NPI. The most common non-standard attributes are:

- `geographical_area` – seems to overlap with the keyword `area` already defined in ACDD.
- `instrument` – explaining the instrument(s) used for measurement or “unknown”; e.g. “SBE 911plus CTD”, “Aanderaa RCM 8”, ...
- `instrument_type` – the type of the instrument, e.g. “Upward Looking Sonar”, “CTD”, ...
- `location` – seems to be a more detailed description of the place on Earth than `area`, which is defined in ACDD. Where `area` is a rough description (“North Atlantic Ocean”), `location` is more detailed (“Kongsfjorden”). Might use the NPI placenames where possible.
- `standard_name_vocabulary` – the vocabulary used to define the `standard_name` attribute of variables ([see below](#Variable-Attributes)). Something like “CF Standard Name Table v62”.
- `serial_number` was suggested as global attribute for internal use to further define the instrument used to record the data.


Variable Attributes
-------------------

*This section is non-normative. It only summarizes the most important variable names and properties of the CF and ACDD standards.*

Each standardized variable should have at least the attributes `standard_name` and `long_name`. The `standard_name` should be one of the [CF standard names](http://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html). The `long_name` may be any sensible name or description of the variable. Any variables that do not have a standardized name (e.g. serial number, Niskin bottle number) *must not* have the attribute `standard_name`.

Variables may have a `precision` attribute stating the prescision of the device used to record the data. Where appropriate, variables should have an attribute `_FillValue` to indicate missing values.

All variables that represent dimensional quantities should have the attribute `units`. The value of `units` should be recognised by the [Unidata Udunits package](https://www.unidata.ucar.edu/software/udunits). Usually, these are either common unit names (e.g. “meter” or “degree Celsius”) or actual units (e.g. “g kg-1” for salinity).

Variables that represent axes (usually longitude, latitude, depth/pressure and time) should have an `axis` attribute with the values X, Y, Z and T, respectively.

According to CF, time must be given in “*timeunits* since *some datetime*”, e.g. “minutes since 2010-01-01T00:00:00Z”. Absolute time points are not possible according to the standard.

Typical variables in physical oceanography will be the following.

| type | standard_name | units | axis | comment |
| ---- | ------------- | ----- | ---- | ------- |
| all | longitude | degree_east | X | Unit may be degree |
| all | latitude | degree_north | Y | Unit may be degree |
| CTD/bottle | depth | m | Z | If no pressure available |
| all | sea_water_pressure_due_to_sea_water | dbar | Z | Use `sea_water_pressure` if air pressure is included |
| all | time | *timeunit* since *date* | T | date as [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) in UTC |
| all | sea_water_temperature | degree_C or K |  | If possible, use the ITS-90 scale |
| all | sea_water_practical_salinity | g kg-1 |  | Alternatively, use `sea_water_salinity`; if possible, use the PSS-78 scale |
| bottle | moles_of_nitrate_per_unit_mass_in_sea_water | mol kg-1 |  | Replace `nitrate` with other possible chemicals; this is mola**l**ity |
| bottle | mole_concentration_of_dissolved_molecular_oxygen_in_sea_water | mol m-3 |  | Replace `molecular_oxygen` with other possible chemicals; this is mola**r**ity |
| mooring | sea_water_to_direction | degree |  | direction of seawater measured positive clockwise from due north; use in combination with velocity |
| mooring | radial_sea_water_velocity_away_from_instrument | m s-1 |  | use in combination with direction |
| mooring | eastward_sea_water_velocity | m s-1 |  | use in combination with northward velocity |
| mooring | northward_sea_water_velocity | m s-1 |  | use in combination with eastward velocity |
| mooring | sea_floor_depth_below_sea_surface | m s-1 |  | from echosounder |


Possible future plans
---------------------

*This section is non-normative*

In the data catalogue, we save metadata in json files. To minimize (meta)data duplication, it is highly desirable to automatically extract metadata from the uploaded files or inject metadata into the downloadable data files from the json files. However, both options are not implemented as of today and will likely not be implemented within the next year. So, for the time being, the duplication of metadata must be accepted.


Links to various standards
--------------------------

*This section is non-normative*

Following are some international and national guidelines for your information and comparison. The only mandatory standards are the **CF** and **ACDD** metadata conventions as stated above.

- The [NetCDF Climate and Forecast (**CF**) Metadata Conventions](http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html) is the *de facto* standard for metadata definitions in earth sciences. It defines some important global and variable attributes and the *standard names* for variables.

- The [Attribute Convention for Data Discovery (**ACDD**)](http://wiki.esipfed.org/index.php/Attribute_Convention_for_Data_Discovery) builds upon CF and extends it by more global attributes.

- The [Directory Interchange Format (**DIF**) Standard](https://earthdata.nasa.gov/user-resources/standards-and-references/directory-interchange-format-dif-standard) is a mature metadata catalog for Earth sciences.

- The [Global Change Master Directory (**GCMD**)](https://earthdata.nasa.gov/about/gcmd/global-change-master-directory-gcmd-keywords) offers several controlled vocabularies for categorisation of data.

- The [**OceanSITES** format](http://www.oceansites.org/docs/oceansites_data_format_reference_manual.pdf) is a worldwide system of deepwater reference stations.

- [**SeaDataNet**](https://www.seadatanet.org) is a European meta-database for marine data management.

- The [Norwegian Marine Data Centre (**NMDC**)](https://www.nmdc.no) defines data formats and metadata structures that shall be used to comply with them.

- **ISO 19115** is an extensive document behind a paywall that defines metadata for geographic information. Although it was dismissed for our purpose because it is too complex, parts of it are considered by the others standards like CF.


Abbreviations
-------------

*This section is non-normative*

|      |                                             |
| ---- | ------------------------------------------- |
| ACDD | Attribute Convention for Data Discovery     |
| CF   | Climate and Forecast (Metadata Conventions) |
| DIF  | Directory Interchange Format                |
| GCMD | Global Change Master Directory              |
| GDAC | Global Data Assembly Centre                 |
| NMDC | Norwegian Marine Data Centre                |
| NPDC | Norwegian Polar Institute Data Centre       |
| NPI  | Norwegian Polar Institute                   |
| OS   | OceanSITES                                  |
